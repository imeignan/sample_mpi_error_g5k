from execo import *
from execo_g5k import *
from execo_engine import Engine
import time
import os
import sys

def install_softwares(hosts):
    _ = Remote("sudo-g5k su root -c 'echo 1 > /proc/sys/kernel/unprivileged_userns_clone' && curl -L https://nixos.org/nix/install | sh", hosts).run()
    _ = Remote("~/.nix-profile/bin/nix-env -f ~/nur-kapack -iA npb -I ~/.nix-defexpr/channels/", hosts).run()

    print("Softwares installed : -- %s seconds --\nBeginning bug sample" % (time.time()-starttime))

class Bench(Engine):

    def prepare_bench(self):
        """Request a job to g5k and set up the nodes to execute the benchmark"""
        self.site="grenoble"

        #Requests a job
        self.jobs = oarsub([(OarSubmission(resources="cluster=1/nodes=2", walltime="0:30:0", additional_options="-O /dev/null -E /dev/null"), self.site)])
        self.job=self.jobs[0]
        job_id, site = self.job
        wait_oar_job_start(job_id)
        # After job is started, recover hostnames
        print("job started : -- %s seconds --" % (time.time()-starttime))
        self.nodes = get_oar_job_nodes(job_id, site)
        print(self.nodes)
       
        install_softwares(self.nodes)     
        
    
    def run_xp(self):
        """Execute the bench for a given combination of parameters."""
        

        bench_command = "ulimit -s unlimited && mpirun -machinefile $OAR_NODEFILE -mca mtl psm2 -mca pml ^ucx,ofi -mca btl ^ofi,openib ~/.nix-profile/bin/ep.E.mpi"
    
        p = SshProcess(bench_command, self.nodes[0]).run(timeout=300)
        print("One done")


if __name__ == "__main__":
    starttime=time.time()
    if( len(sys.argv) != 2 ):
        print("Please specify the site where you want to submit the jobs.\npython script.py [site]")
        exit()
    bench = Bench()
    bench.prepare_bench(sys.argv[0])
    for i in range(10):
        bench.run_xp()
